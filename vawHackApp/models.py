from django.db import models

# Create your models here.

class DevelopmentRegion(models.Model):
	region_name = models.CharField(max_length = 100, blank = True)


class Zone(models.Model):
	zone_name = models.CharField(max_length = 100, blank = True)
	region = models.ForeignKey(DevelopmentRegion, blank = True)


class District(models.Model):
	district_name = models.CharField(max_length = 100, blank = True)
	zone = models.ForeignKey(Zone, blank = True)
	latitude = models.FloatField(null = True)
	longitude = models.FloatField(null = True)


class Location(models.Model):
	location_name = models.CharField(max_length = 100, blank = True)
	district = models.ForeignKey(District, blank = True)


class Category(models.Model):
	category_name = models.CharField(max_length = 100, blank = True)


class  SourceType(models.Model):
	source_type_name = models.CharField(max_length = 100, blank = True)


class Source(models.Model):
	source_name = models.CharField(max_length = 100, blank = True)
	source_type = models.ForeignKey(SourceType, blank = True)
	source_date = models.DateField(blank = True)
	source_email = models.EmailField(max_length = 75, blank = True)
	source_phone = models.PositiveIntegerField(null = True)
	

''' incident_image added'''
class Incident(models.Model):
	incident_title = models.CharField(max_length = 100, blank = True)
	incident_date = models.DateField(blank = True)
	incident_time = models.TimeField(blank = True)
	incident_district = models.ForeignKey(District, blank = True)
	incident_category = models.ManyToManyField(Category, blank = True, null=True)
	incident_source = models.ForeignKey(Source, blank = True, null=True)
	incident_source_url = models.URLField(max_length = 200, blank = True)
	death_number = models.PositiveIntegerField(null = True)
	injured_number = models.PositiveIntegerField(null = True)
	verified = models.NullBooleanField()
	description = models.TextField(blank = True)


class NGO(models.Model):
	ngo_name = models.CharField(max_length = 200, blank = True)
	ngo_location = models.ForeignKey(District, null = True)
	ngo_contact_person = models.CharField(max_length = 200, blank = True)
	ngo_contact = models.CharField(max_length = 200, blank = True)
