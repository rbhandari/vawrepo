from django.conf import settings
from vawHackApp.models import *
import csv
import json
import os
from views_scrape_news import *
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt  
from views_send_twitter import * 
from django.utils.encoding import smart_str, smart_unicode

def ngo_map(request):
	return render_to_response("mapping.html")