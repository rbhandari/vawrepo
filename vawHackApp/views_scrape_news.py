import urllib
import lxml.html

def ekantipur_parser(given_url):
	try:
		data_source = urllib.urlopen(given_url)
		html = data_source.read()
		root = lxml.html.fromstring(html)
		news ={}
		news["location"] = root.cssselect('div.main_wrapper_left p')[0].text_content().split(',')[0]
		description = root.cssselect('div.main_wrapper_left')[0]
		news["description"] = description.text_content().split("Posted on")[0]
	except:
		return 0
	return news

def news_ekantipur(keyword):
	xml_url = "https://news.google.com/news/feeds?q="+keyword+"+inurl%3A%22ekantipur.com%22&output=atom"
	
	data_source = urllib.urlopen(xml_url)
	my_data = data_source.read()
	data = lxml.html.fromstring(str(my_data))
	entries = data.xpath("//entry")
	news_items =[]
	for each_entry in entries:
		news ={}
		news["title"] = each_entry.cssselect("title")[0].text_content()
		date_time = each_entry.cssselect("updated")[0].text_content() 
		news["date"] = date_time.split('T')[0]
		news["time"] = date_time.split('T')[1].replace("Z","")
		news["url"] = each_entry.cssselect("id")[0].text_content().split("cluster=")[-1]
		news_items.append(news)

	final_items =[]
	for each_news in news_items:
		new_news = each_news
		obtained_news = ekantipur_parser(each_news["url"])
		if obtained_news!=0:
			new_news["location"] = obtained_news["location"]
			new_news["description"] = obtained_news["description"]
			final_items.append(new_news)
	return final_items