from django.conf.urls import patterns, include, url
from vawHackApp.views import *
from vawHackApp.mapping_view import *
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^load-location/?$',location_coordinate),
    url(r'^load-ngo/?$',NGO_load),
    url(r'^loadDataFromINSEC/?$',scrap_insec),
    url(r'^loadEkantipur/?$',scrap_ekantipur),
    #url(r'^load-news-data/?$',add_news),
    # url(r'^incident/(?P<incident_id>.*?)/?$', incident_page),
    # url(r'^incident-json/?$',incident_json),
    # url(r'^ngo-json/?$',ngo_json),
    # url(r'^incident-csv/?$',incident_csv),
    # url(r'^ngo-csv/?$',ngo_csv),
    # url(r'^map-ngo/?$',ngo_map),
    # url(r'^loadDataFromNepalMonitor/?$',scrap_nepalMonitor),
    # url(r'^$', 'vawHack.views.home', name='home'),
    # url(r'^vawHack/', include('vawHack.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
